#!/bin/sh

set -e

function install_dependencies {
  apk add --update curl git make tar xz
  git clone https://github.com/oracle/smith && \
    cd smith && \
    git checkout v1.0.0 && \
    make install && \
    cd ..
}

function build {
  smith
}

function upload {
  smith upload -d -i image.tar.gz -r "https://gitlab-ci-token:$CI_JOB_TOKEN@$CI_REGISTRY_IMAGE/micro"
}

install_dependencies
build
upload
